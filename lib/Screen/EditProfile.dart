
import 'dart:io';


import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';

import '../Model/UserModel.dart';
import '../Pages/Profile.dart';

class EditProfile extends StatefulWidget {



  const EditProfile({Key? key}) : super(key: key);

  @override
  State<EditProfile> createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {
  TextEditingController userName = TextEditingController();
  TextEditingController userEmail = TextEditingController();
  TextEditingController userPhone = TextEditingController();

  bool isLoading = false;

  String? getImageUrl = "";

  PickedFile? _imageFile;
  final ImagePicker _picker = ImagePicker();

  UserModel? userModel;

  @override
  void initState() {
    retriveData();
    super.initState();
    retriveData().then((value) {
      userModel = value;
      userName.text = userModel?.userName ?? "";
      userEmail.text = userModel?.userEmail ?? "";
      userPhone.text = userModel?.userPhone ?? "";
    });
  }

  void updateUser() async {
    var auth = FirebaseAuth.instance.currentUser?.uid;
    await uploadImageToFirebase();
    FirebaseFirestore.instance.collection("users").doc(auth).update({
      "userName": userName.text.trim(),
      "userEmail": userEmail.text.trim(),
      "userPhone": userPhone.text.trim(),
      "imageUrl": getImageUrl
    }).then((value) {
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => const Profile(),));
      Fluttertoast.showToast(msg: "Update Successful");
    });
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        title: const Text("Edit Profile"),
        leading: IconButton(
          icon: const Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                builder: (context) => const Profile(),
              )),
        ),
      ),
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 30),
            child: Column(
              children: [
                Stack(
                    children: [
                      _imageFile != null ?
                      CircleAvatar(
                          radius: 50,
                          backgroundImage: FileImage(File(_imageFile!.path))
                      ):
                      CircleAvatar(
                        radius: 50,
                        backgroundImage: userModel?.imageUrl != ""? NetworkImage("${userModel?.imageUrl}") : const NetworkImage("https://pixlok.com/wp-content/uploads/2022/02/Profile-Icon-SVG-09856789.png"),
                        // widget.imagePath != null ? FileImage(File(imageFile!.path)) : null,
                      ),
                  Positioned(
                      bottom: 0,
                      right: 0,
                      child: Container(
                        width: 35,
                        height: 35,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(100),
                            color: Colors.white),
                        child: IconButton(
                          icon: const Icon(Icons.camera_alt),
                          onPressed: () {
                            showModalBottomSheet(
                                context: context,
                                builder: (BuildContext context) {
                                  return Wrap(
                                    children: [
                                      ListTile(
                                        leading: const Icon(
                                          Icons.camera_alt,
                                          color: Colors.black,
                                        ),
                                        title: Text('Camera'),
                                        onTap: () {
                                          takePhoto(ImageSource.camera);
                                          Navigator.pop(context);
                                        },
                                      ),
                                      ListTile(
                                        leading: const Icon(
                                          Icons.image,
                                          color: Colors.black,
                                        ),
                                        title: Text('Gallery'),
                                        onTap: () {
                                          takePhoto(ImageSource.gallery);
                                          Navigator.pop(context);
                                        },
                                      ),
                                    ],
                                  );
                                });
                          },
                        ),
                      )),
                ]),
                Padding(
                  padding: const EdgeInsets.only(top: 30),
                  child: Container(
                    // padding: EdgeInsets.all(4),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: const [
                          BoxShadow(
                              color: Color.fromRGBO(143, 148, 251, .2),
                              blurRadius: 20.0,
                              offset: Offset(0, 10))
                        ]),
                    child: Column(
                      children: [
                        Container(
                          // padding: EdgeInsets.all(8),
                          decoration: const BoxDecoration(
                              color: Colors.white,
                              border: Border(
                                  bottom: BorderSide(color: Colors.grey))),
                          child: TextField(
                            controller: userName,
                            decoration: InputDecoration(
                                prefixIcon: Icon(Icons.person),
                                // border: InputBorder.none,
                                labelText: "Name",
                                hintStyle: TextStyle(color: Colors.grey[400])),
                            keyboardType: TextInputType.text,
                          ),
                        ),
                        Container(
                          // padding: EdgeInsets.all(5),
                          decoration: const BoxDecoration(
                              color: Colors.white,
                              border: Border(
                                  bottom: BorderSide(color: Colors.grey))),
                          child: TextField(
                            controller: userPhone,
                            decoration: InputDecoration(
                                prefixIcon: Icon(Icons.phone),
                                border: InputBorder.none,
                                labelText: "Phone Number",
                                hintStyle: TextStyle(color: Colors.grey[400])),
                            keyboardType: TextInputType.number,
                            maxLength: 10,
                          ),
                        ),
                        Container(
                          // padding: EdgeInsets.all(8),
                          decoration: const BoxDecoration(
                              color: Colors.white,
                              border: Border(
                                  bottom: BorderSide(color: Colors.grey))),
                          child: TextField(
                            controller: userEmail,
                            decoration: InputDecoration(
                                prefixIcon: Icon(Icons.email_outlined),
                                // border: InputBorder.none,
                                labelText: "Email",
                                hintStyle: TextStyle(color: Colors.grey[400])),
                            keyboardType: TextInputType.emailAddress,
                          ),
                        ),

                        const SizedBox(
                          height: 40,
                        ),
                        Container(
                          height: 44.0,
                          width: 300,
                          decoration: const BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5)),
                              gradient: LinearGradient(colors: [
                                Color.fromARGB(255, 2, 173, 102),
                                Colors.blue
                              ])),
                          child: ElevatedButton(
                            onPressed: () {
                              setState(() {
                                isLoading = true;
                              });
                              Future.delayed(const Duration(seconds: 3), () {
                                setState(() {
                                  isLoading = false;
                                });
                              });

                              updateUser();
                            },
                            style: ElevatedButton.styleFrom(
                                backgroundColor: Colors.transparent,
                                shadowColor: Colors.transparent),
                            child: isLoading
                                ? Row(
                                    mainAxisAlignment: MainAxisAlignment.center,

                                    // ignore: prefer_const_literals_to_create_immutables
                                    children: [
                                      const Text(
                                        'Loading...',
                                        style: TextStyle(fontSize: 20),
                                      ),
                                      const SizedBox(width: 5),
                                      const CircularProgressIndicator(
                                        color: Colors.white,
                                        strokeWidth: 3,
                                      ),
                                    ],
                                  )
                                : const Text('Update Profile'),
                          ),
                        ),
                        // const SizedBox(
                        //   height: 10,
                        // ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }


  void takePhoto(ImageSource source) async {
    final pickedFile = await _picker.getImage(
      source: source,
    );
    setState(() {
      _imageFile = pickedFile;
    });
  }

  Future<void> uploadImageToFirebase() async {
    final auth = FirebaseAuth.instance.currentUser?.uid;
    if (_imageFile != null) {
      File file = File(_imageFile!.path);
      var ref = FirebaseStorage.instance
          .ref()
          .child('usersImage')
          .child('$auth.jpg');
      UploadTask uploadTask = ref.putFile(file);
      TaskSnapshot snapshot = await uploadTask;
      getImageUrl = await snapshot.ref.getDownloadURL();
    }
  }


  Future<UserModel?> retriveData() async {
    var auth = FirebaseAuth.instance.currentUser?.uid;
    var user = await FirebaseFirestore.instance.collection("users").doc(auth).get();
    var usermodal = UserModel.fromJson(user.data()!);
    return usermodal;
  }



}
