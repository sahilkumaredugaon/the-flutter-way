import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../Pages/Cart.dart';
import '../Pages/Categories.dart';
import '../Pages/Home.dart';
import '../Pages/Profile.dart';

class Navigation extends StatefulWidget {
  const Navigation({Key? key}) : super(key: key);

  @override
  State<Navigation> createState() => _NavigationState();
}

class _NavigationState extends State<Navigation> {
  int selectedIndex = 0;

  final pages = [
    const HomePage(),
    const Categories(),
    const Cart(),
    const Profile(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:pages[selectedIndex],
        bottomNavigationBar: BottomNavigationBar(
          currentIndex: selectedIndex,
          items: const [
            BottomNavigationBarItem(
              icon:Icon(Icons.home),
              backgroundColor: Colors.black,
              label: "Home",
            ),
            BottomNavigationBarItem(
              icon:Icon(Icons.category),
              label: "Categories",
              backgroundColor: Colors.red,
            ),
            BottomNavigationBarItem(
              icon:Icon(Icons.add_shopping_cart_outlined),
              backgroundColor: Colors.green,
              label: "Cart",
            ),
            BottomNavigationBarItem(
              icon:Icon(Icons.person),
              backgroundColor: Colors.blue,
              label: "Profile",
            ),
          ],
          onTap: (index){
            setState(() {
              selectedIndex=index;
            });
          },
        )
    );
  }
}
