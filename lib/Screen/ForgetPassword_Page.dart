import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class ForgetPage extends StatefulWidget {
  const ForgetPage({Key? key}) : super(key: key);

  @override
  State<ForgetPage> createState() => _ForgetPageState();
}

class _ForgetPageState extends State<ForgetPage> {
  TextEditingController forgetPasswordController = TextEditingController();

  bool isLoading = false;

  Future ForgetPassword() async {
    var forgetEmail = forgetPasswordController.text.trim();

    try {
      await FirebaseAuth.instance
          .sendPasswordResetEmail(email: forgetEmail)
          .then((value) => {
                Fluttertoast.showToast(msg: "Email sent"),
                Navigator.pop(context),
              });
    } on FirebaseAuthException catch (e) {
      Fluttertoast.showToast(msg: "${e}");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Forget Password"),
        centerTitle: true,
        leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: const Icon(Icons.arrow_back)),
      ),
      body: ListView(
        children: [
          Stack(
            children: [
              Container(
                height: 200,
                color: Color.fromRGBO(157, 69, 225, 1.0),
              ),
              Center(
                child: Padding(
                  padding: const EdgeInsets.only(top: 80),
                  child: Container(
                    height: 400,
                    width: 400,
                    child: Card(
                      elevation: 10,
                      margin: EdgeInsets.all(20),
                      child: Column(
                        children: [
                          const SizedBox(
                              height: 100,
                              width: 100,
                              child: Padding(
                                padding: EdgeInsets.only(top: 10),
                                child: Image(
                                    image: AssetImage(
                                        "assets/images/forgotPassword.png")),
                              )),
                          const Padding(
                            padding: EdgeInsets.only(top: 20),
                            child: Text(
                              "Forgot Password",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          const Padding(
                              padding:
                                  EdgeInsets.only(top: 10, right: 30, left: 30),
                              child: Text(
                                "Enter your email and we'll send your a link to reset your password",
                                style: TextStyle(
                                  color: Color.fromRGBO(183, 179, 179, 1.0),
                                  fontSize: 13,
                                ),
                              )),
                          Container(
                            padding: EdgeInsets.all(8),
                            decoration: const BoxDecoration(
                              color: Colors.white,
                            ),
                            child: TextField(
                              controller: forgetPasswordController,
                              decoration: InputDecoration(
                                  prefixIcon: Icon(Icons.email_outlined),
                                  // border: InputBorder.none,
                                  hintText: "Email",
                                  labelText: "Email",
                                  hintStyle:
                                      TextStyle(color: Colors.grey[400])),
                              keyboardType: TextInputType.emailAddress,
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 40),
                            height: 44.0,
                            width: 300,
                            decoration: const BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(5)),
                                gradient: LinearGradient(colors: [
                                  Color.fromARGB(255, 2, 173, 102),
                                  Colors.blue
                                ])),
                            child: ElevatedButton(
                              onPressed: () {
                                setState(() {
                                  isLoading = true;
                                });
                                Future.delayed(const Duration(seconds: 3), () {
                                  setState(() {
                                    isLoading = false;
                                  });
                                });

                                ForgetPassword();
                              },
                              style: ElevatedButton.styleFrom(
                                  backgroundColor: Colors.transparent,
                                  shadowColor: Colors.transparent),
                              child: isLoading
                                  ? Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,

                                      // ignore: prefer_const_literals_to_create_immutables
                                      children: [
                                        const Text(
                                          'Loading...',
                                          style: TextStyle(fontSize: 20),
                                        ),
                                        const SizedBox(width: 5),
                                        const CircularProgressIndicator(
                                          color: Colors.white,
                                          strokeWidth: 3,
                                        ),
                                      ],
                                    )
                                  : const Text('Forget Password'),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
