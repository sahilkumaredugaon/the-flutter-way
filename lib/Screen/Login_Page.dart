import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_sign_in/google_sign_in.dart';

import '../PhoneAuth/PhoneNumber.dart';
import 'ForgetPassword_Page.dart';
import 'Navigation_Page.dart';
import 'SignUp_Page.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {

  TextEditingController emailController=TextEditingController();
  TextEditingController passwordController=TextEditingController();

  bool passwordVisible = false;
  bool isLoading = false;

  validateForm() {
    if (!emailController.text.contains("@gmail.com")) {
      Fluttertoast.showToast(msg: 'email is inValid');
    } else if (passwordController.text.length < 5) {
      Fluttertoast.showToast(msg: 'password min 5 letters');
    } else {
      userLogin();
    }
  }

  userLogin() async {
    var mAuth = FirebaseAuth.instance;
    try {
      await mAuth.signInWithEmailAndPassword(
          email: emailController.text, password: passwordController.text);
      Fluttertoast.showToast(
          msg: "Login Successful ", textColor: Colors.yellowAccent);

      // ignore: use_build_context_synchronously
      Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (context) => const Navigation()));
    } catch (e) {
      Fluttertoast.showToast(msg: "Invalid email and password");
    }

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Scaffold(
        backgroundColor: Colors.white,
        body: ListView(
          children: [
            Container(
              height: 250,
              decoration: const BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('assets/images/img_1.png'),
                      fit: BoxFit.fill)),
              child: Stack(
                children: [
                  Positioned(
                    width: 80,
                    left: 30,
                    height: 200,
                    child: Container(
                      decoration: const BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage('assets/images/img_2.png'))),
                    ),
                  ),
                  Positioned(
                      width: 80,
                      left: 140,
                      height: 150,
                      child: Container(
                        decoration: const BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage('assets/images/light-2.png'))),
                      )),
                  Positioned(
                    right: 40,
                    top: 40,
                    width: 80,
                    height: 150,
                    child: Container(
                      decoration: const BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage('assets/images/clock.png'))),
                    ),
                  ),
                  Positioned(
                    child: Container(
                      margin: EdgeInsets.only(top: 150, left: 120),
                      child: const Text(
                        "Login",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 40,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  )
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: 20, right: 20),
              child: Column(
                children: [
                  Container(
                    padding: const EdgeInsets.all(4),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: const [
                          BoxShadow(
                              color: Color.fromRGBO(143, 148, 251, .2),
                              blurRadius: 20.0,
                              offset: Offset(0, 10))
                        ]),
                    child: Column(
                      children: [
                        Container(
                          padding: const EdgeInsets.all(8),
                          decoration: const BoxDecoration(
                              color: Colors.white,
                              border:
                              Border(bottom: BorderSide(color: Colors.grey))),
                          child: TextField(
                            controller: emailController,
                            decoration: InputDecoration(
                                prefixIcon: const Icon(Icons.email_outlined),
                                border: InputBorder.none,
                                hintText: "Email",
                                labelText: "Email",
                                hintStyle: TextStyle(color: Colors.grey[400])),
                            onChanged: (value) {
                              setState(() {
                                // _email = value.trim();
                              });
                            },
                            keyboardType: TextInputType.emailAddress,
                            textInputAction: TextInputAction.done,
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.all(8),
                          child: TextField(
                            controller: passwordController,
                            obscureText: passwordVisible,
                            decoration: InputDecoration(
                              prefixIcon: Icon(Icons.key_outlined),
                              border: InputBorder.none,
                              hintText: "Password",
                              labelText: "Password",
                              helperText:
                              "Password must contain special character",
                              hintStyle: TextStyle(color: Colors.grey[400]),
                              suffixIcon: IconButton(
                                icon: Icon(passwordVisible
                                    ? Icons.visibility
                                    : Icons.visibility_off),
                                onPressed: () {
                                  setState(
                                        () {
                                      passwordVisible = !passwordVisible;
                                    },
                                  );
                                },
                              ),
                            ),
                            onChanged: (value) {
                              setState(() {
                                // _password = value.trim();
                              });
                            },
                            keyboardType: TextInputType.visiblePassword,
                            textInputAction: TextInputAction.done,
                            maxLength: 15,
                          ),
                        ),
                        Row(
                          children: [
                            Container(
                                margin: const EdgeInsets.only(left: 180),
                                child: TextButton(
                                    onPressed: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                            builder: (context) =>
                                            const ForgetPage(),
                                          ));
                                    },
                                    child: const Text("Forgot Password?"))),
                          ],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Container(
                          height: 47.0,
                          width: 300,
                          decoration: const BoxDecoration(
                              borderRadius: BorderRadius.all(Radius.circular(5)),
                              gradient: LinearGradient(colors: [
                                Color.fromARGB(255, 2, 173, 102),
                                Colors.blue
                              ])),
                          child: ElevatedButton(
                            onPressed: () {
                              setState(() {
                                isLoading = true;
                              });
                              Future.delayed(const Duration(seconds: 3), () {
                                setState(() {
                                  isLoading = false;
                                });
                              });
                              validateForm();
                              },
                            style: ElevatedButton.styleFrom(
                                backgroundColor: Colors.transparent,
                                shadowColor: Colors.transparent),
                            child: isLoading
                                ? Row(
                              mainAxisAlignment: MainAxisAlignment.center,

                              // ignore: prefer_const_literals_to_create_immutables
                              children: [
                                const Text(
                                  'Loading...',
                                  style: TextStyle(fontSize: 20),
                                ),
                                const SizedBox(width: 5),
                                const CircularProgressIndicator(
                                  color: Colors.white,
                                  strokeWidth: 3,
                                ),
                              ],
                            )
                                : const Text('Sign In'),
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            const Text("Don't Have Any Account?  "),
                            InkWell(
                              onTap: () {
                                Navigator.pushReplacement(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => SignUpPage(),
                                    ));
                              },
                              child: const Text("Sign Up",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.blue)),
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              margin: EdgeInsets.only(bottom: 20),
                              height: 40,
                              width: 40,
                              child: GestureDetector(
                                child: Image.asset(
                                  'assets/images/fb.png',
                                ),
                                onTap: () {},
                              ),
                            ),
                            const SizedBox(
                              width: 35,
                            ),
                            Container(
                              margin: EdgeInsets.only(bottom: 20),
                              height: 45,
                              width: 45,
                              child: GestureDetector(
                                child: Image.asset(
                                  'assets/images/goggle.png',
                                ),
                                onTap: () {
                                  signInWithGoogle().then((value) {
                                    Navigator.pushReplacement(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) => const Navigation(),
                                        ));
                                    Fluttertoast.showToast(
                                        msg: "Login successful");
                                  });

                                },
                              ),
                            ),
                            const SizedBox(
                              width: 30,
                            ),
                            Container(
                              margin: EdgeInsets.only(bottom: 20),
                              height: 50,
                              width: 50,
                              child: GestureDetector(
                                child: Image.asset('assets/images/img_3.png'),
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => const PhoneAuth(),
                                      ));
                                  Fluttertoast.showToast(
                                      msg: "Phone Verification");
                                },
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Future<UserCredential> signInWithGoogle() async {
    // Trigger the authentication flow
    final GoogleSignInAccount? googleUser = await GoogleSignIn().signIn();

    // Obtain the auth details from the request
    final GoogleSignInAuthentication? googleAuth = await googleUser?.authentication;

    // Create a new credential
    final credential = GoogleAuthProvider.credential(
      accessToken: googleAuth?.accessToken,
      idToken: googleAuth?.idToken,
    );

    // Once signed in, return the UserCredential
    return await FirebaseAuth.instance.signInWithCredential(credential);
  }
}
