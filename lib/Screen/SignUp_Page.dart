import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'Login_Page.dart';
import 'Navigation_Page.dart';

class SignUpPage extends StatefulWidget {
  const SignUpPage({Key? key}) : super(key: key);

  @override
  State<SignUpPage> createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  bool passwordVisible = false;
  bool isLoading = false;

  TextEditingController userName = TextEditingController();
  TextEditingController userEmail = TextEditingController();
  TextEditingController userPhone = TextEditingController();
  TextEditingController userPassword = TextEditingController();

  validateFrom() {
    if (userName.text.length < 3) {
      Fluttertoast.showToast(msg: 'Please All Fill Data');
    } else if (userPhone.text.length < 10) {
      Fluttertoast.showToast(msg: 'number max size 10 characters');
    } else if (!userEmail.text.contains("@gmail.com")) {
      Fluttertoast.showToast(msg: 'email is inValid');
    } else if (userPassword.text.length < 5) {
      Fluttertoast.showToast(msg: 'password min 5 letters');
    } else {
      userEmailAuth();
    }
  }

  userEmailAuth() {
    FirebaseAuth.instance
        .createUserWithEmailAndPassword(
            email: userEmail.text, password: userPassword.text)
        .then((value) {
      userFireStore();
    });
  }

  userFireStore() {
    var userId = FirebaseAuth.instance.currentUser?.uid;
    FirebaseFirestore.instance.collection("users").doc(userId).set({
      "userName": userName.text.trim(),
      "userEmail": userEmail.text.trim(),
      "userPhone": userPhone.text.trim(),
      "userPassword": userPassword.text.trim(),
    }).then((value) {
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => const Navigation(),
          ));
      Fluttertoast.showToast(msg: "Successful");
    }).catchError((error) {
      Fluttertoast.showToast(msg: 'Failed');
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          Container(
            height: 230,
            decoration: const BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('assets/images/img_1.png'),
                    fit: BoxFit.fill)),
            child: Stack(
              children: [
                Positioned(
                  width: 80,
                  left: 30,
                  height: 200,
                  child: Container(
                    decoration: const BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage('assets/images/img_2.png'))),
                  ),
                ),
                Positioned(
                    width: 80,
                    left: 140,
                    height: 150,
                    child: Container(
                      decoration: const BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage('assets/images/light-2.png'))),
                    )),
                Positioned(
                  right: 40,
                  top: 40,
                  width: 80,
                  height: 150,
                  child: Container(
                    decoration: const BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage('assets/images/clock.png'))),
                  ),
                ),
                Positioned(
                  child: Container(
                    margin: EdgeInsets.only(top: 140, left: 120),
                    child: const Text(
                      "Sign Up",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 40,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                )
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: 20, right: 20),
            child: Column(
              children: [
                Container(
                  // padding: EdgeInsets.all(4),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: const [
                        BoxShadow(
                            color: Color.fromRGBO(143, 148, 251, .2),
                            blurRadius: 20.0,
                            offset: Offset(0, 10))
                      ]),
                  child: Column(
                    children: [
                      Container(
                        // padding: EdgeInsets.all(8),
                        decoration: const BoxDecoration(
                            color: Colors.white,
                            border:
                                Border(bottom: BorderSide(color: Colors.grey))),
                        child: TextField(
                          controller: userName,
                          decoration: InputDecoration(
                              prefixIcon: Icon(Icons.person),
                              // border: InputBorder.none,
                              hintText: "Name",
                              labelText: "Name",
                              hintStyle: TextStyle(color: Colors.grey[400])),
                          keyboardType: TextInputType.text,
                        ),
                      ),
                      Container(
                        // padding: EdgeInsets.all(5),
                        decoration: const BoxDecoration(
                            color: Colors.white,
                            border:
                                Border(bottom: BorderSide(color: Colors.grey))),
                        child: TextField(
                          controller: userPhone,
                          decoration: InputDecoration(
                              prefixIcon: const Icon(Icons.phone),
                              border: InputBorder.none,
                              hintText: "Phone Number",
                              labelText: "Phone Number",
                              hintStyle: TextStyle(color: Colors.grey[400])),
                          keyboardType: TextInputType.number,
                          maxLength: 10,
                        ),
                      ),
                      Container(
                        // padding: EdgeInsets.all(8),
                        decoration: const BoxDecoration(
                            color: Colors.white,
                            border:
                                Border(bottom: BorderSide(color: Colors.grey))),
                        child: TextField(
                          controller: userEmail,
                          decoration: InputDecoration(
                              prefixIcon: Icon(Icons.email_outlined),
                              // border: InputBorder.none,
                              hintText: "Email",
                              labelText: "Email",
                              hintStyle: TextStyle(color: Colors.grey[400])),
                          keyboardType: TextInputType.emailAddress,
                        ),
                      ),
                      TextField(
                        controller: userPassword,
                        obscureText: passwordVisible,
                        decoration: InputDecoration(
                          prefixIcon: const Icon(Icons.key_outlined),
                          border: InputBorder.none,
                          hintText: "Password",
                          labelText: "Password",
                          helperText: "Password must contain special character",
                          hintStyle: TextStyle(color: Colors.grey[400]),
                          suffixIcon: IconButton(
                            icon: Icon(passwordVisible
                                ? Icons.visibility
                                : Icons.visibility_off),
                            onPressed: () {
                              setState(
                                () {
                                  passwordVisible = !passwordVisible;
                                },
                              );
                            },
                          ),
                        ),
                        keyboardType: TextInputType.visiblePassword,
                        textInputAction: TextInputAction.done,
                        maxLength: 15,
                      ),

                      const SizedBox(
                        height: 20,
                      ),
                      Container(
                        height: 44.0,
                        width: 300,
                        decoration: const BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(5)),
                            gradient: LinearGradient(colors: [
                              Color.fromARGB(255, 2, 173, 102),
                              Colors.blue
                            ])),
                        child: ElevatedButton(
                          onPressed: () {
                            setState(() {
                              isLoading = true;
                            });
                            Future.delayed(const Duration(seconds: 3), () {
                              setState(() {
                                isLoading = false;
                              });
                            });

                            validateFrom();
                          },
                          style: ElevatedButton.styleFrom(
                              backgroundColor: Colors.transparent,
                              shadowColor: Colors.transparent),
                          child: isLoading
                              ? Row(
                                  mainAxisAlignment: MainAxisAlignment.center,

                                  // ignore: prefer_const_literals_to_create_immutables
                                  children: [
                                    const Text(
                                      'Loading...',
                                      style: TextStyle(fontSize: 20),
                                    ),
                                    const SizedBox(width: 5),
                                    const CircularProgressIndicator(
                                      color: Colors.white,
                                      strokeWidth: 3,
                                    ),
                                  ],
                                )
                              : const Text('Sign In'),
                        ),
                      ),
                      // const SizedBox(
                      //   height: 10,
                      // ),
                      Padding(
                        padding: const EdgeInsets.all(10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            const Text("Do Have Any Account?  "),
                            InkWell(
                              onTap: () {
                                Navigator.pushReplacement(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => LoginPage(),
                                    ));
                              },
                              child: const Text("Login",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.blue)),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
