import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:the_flutter_way/Model/UserModel.dart';
import 'package:the_flutter_way/Screen/Login_Page.dart';

import '../Screen/EditProfile.dart';

class Profile extends StatefulWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  State<Profile> createState() => _ProfileState();

}

class _ProfileState extends State<Profile> {

  UserModel?userModel;

  @override
  void initState() {
    super.initState();
    retriveData().then((value) {
      userModel = value;
      setState(() {});
    });
  }

  @override
  void dispose() {
    super.dispose();
    setState(() {
      final credential = GoogleAuthProvider.credential(
        accessToken: googleAuth?.accessToken,
        idToken: googleAuth?.idToken,
      );

    });
  }

  FirebaseAuth auth=FirebaseAuth.instance;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          title: const Text('Account'),
          actions: [
            IconButton(
              onPressed: () {},
              icon: (const Icon(Icons.search)),
            ),
            IconButton(
              onPressed: () {},
              icon: (const Icon(Icons.shopping_cart)),
            )
          ],
        ),
        body: ListView(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                  Padding(
                    padding: const EdgeInsets.only(right: 20),
                    child: SizedBox(
                      height: 70,
                      width: 70,
                      child: CircleAvatar(
                        backgroundImage: NetworkImage(userModel?.imageUrl ??""),
                      ),
                    )),
                Padding(
                  padding: const EdgeInsets.only(top: 20),
                  child: Column(
                    children: [
                       Text("${userModel?.userName}",
                          style: const TextStyle(
                              color: Colors.black,
                              fontSize: 20,
                              fontWeight: FontWeight.bold)),
                       Text( "${userModel?.userEmail}",
                          style: TextStyle(color: Colors.black)),
                      ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              backgroundColor: Colors.blue,
                              padding:
                              const EdgeInsets.symmetric(horizontal: 25)),
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>  EditProfile()
                                )
                            );
                          },
                          child: const Text('Edit Profile'))
                    ],
                  ),
                )
              ],
            ),
            const Card(
              elevation: 3,
              child: ListTile(
                  leading: Icon(Icons.help_center),
                  title: Text("Help Center"),
                  trailing: Icon(Icons.arrow_forward_ios_outlined)),
            ),
            const Card(
              elevation: 1,
              child: ListTile(
                  leading: Icon(Icons.language),
                  title: Text("Change language"),
                  trailing: Icon(Icons.arrow_forward_ios_outlined)),
            ),
            const Card(
              elevation: 1,
              child: ListTile(
                  leading: Icon(Icons.location_on_outlined),
                  title: Text("Map Location"),
                  // onTap: ,
                  trailing: Icon(Icons.arrow_forward_ios_outlined)),
            ),
            const Card(
              elevation: 1,
              child: ListTile(
                  leading: Icon(Icons.account_balance_outlined),
                  title: Text("My Bank & UPI Details"),
                  trailing: Icon(Icons.arrow_forward_ios_outlined)),
            ),
            const Card(
              elevation: 1,
              child: ListTile(
                  leading: Icon(Icons.share),
                  title: Text("My Shared Product"),
                  trailing: Icon(Icons.arrow_forward_ios_outlined)),
            ),
            const Card(
              elevation: 1,
              child: ListTile(
                  leading: Icon(Icons.payment),
                  title: Text("My Payments"),
                  trailing: Icon(Icons.arrow_forward_ios_outlined)),
            ),
            const Card(
              elevation: 1,
              child: ListTile(
                  leading: Icon(Icons.card_giftcard),
                  title: Text("Refer & Earn"),
                  trailing: Icon(Icons.arrow_forward_ios_outlined)),
            ),
            const Card(
              elevation: 1,
              child: ListTile(
                  leading: Icon(Icons.settings),
                  title: Text("Settings"),
                  trailing: Icon(Icons.arrow_forward_ios_outlined)),
            ),
            const Card(
              elevation: 1,
              child: ListTile(
                  leading: Icon(Icons.star),
                  title: Text("Rate FlutterGuru"),
                  trailing: Icon(Icons.arrow_forward_ios_outlined)),
            ),
            Card(
              elevation: 1,
              child: ListTile(
                  leading: const Icon(Icons.logout),
                  title: const Text("LogOut"),
                  trailing: const Icon(Icons.arrow_forward_ios_outlined),
                  onTap: () {
                    FirebaseAuth.instance.signOut();
                    Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                          builder: (context) => const LoginPage(),
                        ));
                    Fluttertoast.showToast(msg: "Logout Successful");
                  }),
            ),
          ],
        )
    );
  }

  Future<UserModel?> retriveData() async {
    var auth = FirebaseAuth.instance.currentUser?.uid;
    var user =
    await FirebaseFirestore.instance.collection("users").doc(auth).get();
    var usermodal = UserModel.fromJson(user.data()!);
    return usermodal;
  }
}
