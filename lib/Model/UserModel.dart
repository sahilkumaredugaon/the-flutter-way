// To parse this JSON data, do
//
//     final userModel = userModelFromJson(jsonString);

import 'dart:convert';

UserModel userModelFromJson(String str) => UserModel.fromJson(json.decode(str));

String userModelToJson(UserModel data) => json.encode(data.toJson());

class UserModel {
  String userName;
  String userEmail;
  String userPhone;
  String userPassword;
  String? imageUrl;

  UserModel({
    required this.userName,
    required this.userEmail,
    required this.userPhone,
    required this.userPassword,
    this.imageUrl,
  });

  factory UserModel.fromJson(Map<String, dynamic> json) => UserModel(
    userName: json["userName"],
    userEmail: json["userEmail"],
    userPhone: json["userPhone"],
    userPassword: json["userPassword"],
    imageUrl: json["imageUrl"],
  );

  Map<String, dynamic> toJson() => {
    "userName": userName,
    "userEmail": userEmail,
    "userPhone": userPhone,
    "userPassword": userPassword,
    "imageUrl": imageUrl,
  };
}
